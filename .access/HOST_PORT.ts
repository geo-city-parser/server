import ENV from './isDev';

const obj = {
  dev: {
    HOST: 'localhost',
    PORT: 4012,
    API: 4004,
    MAIL: 4008,
  },
  pub: {
    HOST: 'localhost',
    PORT: 4112,
    API: 4104,
    MAIL: 4108,
  },
};

export default ENV ? obj.dev : obj.pub;
