export type TEnv = any[];
export type TColor = any[];

export interface NodeEnv {
  [prop: string]: (...obj: TEnv) => void;
}
export interface Color {
  (_obj: Object, ...obj: TColor): void;
}
export interface Object {
  prefix?: string | undefined;
  color: string;
}
