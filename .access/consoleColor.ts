import chalk from 'chalk';
import * as I from './Types.d';

const _color: I.Color = (_obj, ...obj) => {
  const { color, prefix } = _obj;
  const _if = prefix ?? '';
  return console.log(chalk.hex(color)(_if, ...obj));
};

export const consoleColor: I.NodeEnv = {
  // colored string,
  info: f => chalk.hex('#4AA0D5')(f),
  dark: f => chalk.hex('#6d7480')(f),
  bug: f => chalk.hex('#E74C3C')(f),
  done: f => chalk.hex('#FFCC66')(f),

  // console log -- empty,
  ok: (...obj) => _color({ color: '#FFCC66' }, ...obj),
  okGray: (...obj) => _color({ color: '#6d7480' }, ...obj),
  color: (...obj) => _color({ color: '#4AA0D5' }, ...obj),
  nodejs: (...obj) => _color({ color: '#689F63' }, ...obj),

  // console log -- prefix,
  nodeEnv: (...obj) => _color({ prefix: '\n⚡ [NODE_ENV]:', color: '#689F63' }, ...obj),
  express: (...obj) => _color({ prefix: '\n⚡ [EXPRESS]:', color: '#689F63' }, ...obj),
  mongodb: (...obj) => _color({ prefix: '\n⚡ [MONGODB]:', color: '#689F63' }, ...obj),

  // colored string -- inverse,
  error: () => chalk.hex('#E74C3C').inverse(`\n⚡ [ERROR]:`),
  reject: () => chalk.hex('#FCE100').inverse(`\n☢️ [PROMISE]:`),
  success: () => chalk.hex('#9ED26A').inverse(`\n⚡ [DONE]:`),
  env: () => chalk.hex('#FC5185').inverse(`\n⚡ [ENV]:`),
  path: () => chalk.hex('#DEADED').inverse(`\n⚡ [PATH]:`),
  server: () => chalk.hex('#E4F9F5').inverse(`\n⚡ [SERVER]:`),

  line: (color: string = 'ok') => consoleColor[color]('─────────────────────────────────────'),
};
