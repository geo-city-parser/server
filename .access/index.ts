import { consoleColor } from './consoleColor';
import { global } from './global';
import HP from './HOST_PORT';
import ENV from './isDev';

const { HOST, PORT, API, MAIL } = HP;
export const _CONSOLE = consoleColor;

// full console log in terminal
export const _DEBUG = { log: false };

export const _NODE = {
  ...{ ENV, HOST, PORT, API },
  url: {
    self: `http://${HOST}:${PORT}` ?? '__TEXT__',
    api: `http://${HOST}:${API}` ?? '__TEXT__',
    mail: `http://${HOST}:${MAIL}` ?? '__TEXT__',
  },
};

// const delQuotes = (str?: string) => String(str).replace(/['"]+/g, '');
const obj = {
  // DEVELOPMENT
  DEV: {},
  // --------------------
  PUB: {},
};
export const _MONGODB = ENV ? obj.DEV : obj.PUB;

export default { ...global };
