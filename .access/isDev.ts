const ENV = process.env.NODE_ENV !== 'production';
export { ENV as default };
