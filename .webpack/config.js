const { resolve } = require('path');

// const root = '../.';
const root = process.cwd();
const dir = resolve(root, 'main');

/**
 * Path
 */
export const _path = {
  src: resolve(dir, 'src'),
  entry: resolve(dir, 'src/index'),
  dist: resolve(dir, 'dist'),
  dotEnv: resolve(root, '.access/.env'),
};

/**
 * Alias
 */
export const aliasSetup = {
  '@root': resolve(root),
  '@main': resolve(dir),
  '@access': resolve(root, '.access'),

  '@app': resolve(dir, 'src/app'),
  '@helper': resolve(dir, 'src/helper'),
  '@logic': resolve(dir, 'src/logic'),
  '@middleware': resolve(dir, 'src/middleware'),
  '@models': resolve(dir, 'src/models'),
  '@routes': resolve(dir, 'src/routes'),
  '@schema': resolve(dir, 'src/schema'),
  '@stash': resolve(dir, 'src/stash'),

  '@pcss': resolve(dir, 'src/assets/postcss'),
};

/**
 * DotEnv
 */
export const confDotenv = {
  path: _path.dotEnv,
  silent: false,
  systemvars: true,
  allowEmptyValues: false,
  expand: true,
  safe: false,
};

export const webpackConf = {
  cache: true,
  parallelism: 100,
  output: {
    publicPath: '/',
    path: _path.dist,
    filename: '[name].js',
    sourceMapFilename: '.map/[file].map[query]',
    hotUpdateChunkFilename: '.hot/[id].[fullhash:5].hot-update.js',
    hotUpdateMainFilename: '.hot/[fullhash:5].hot-update.json',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [{ loader: 'ts-loader', options: { transpileOnly: true } }],
      },
      {
        test: /\.(js|jsx)$/,
        use: ['babel-loader'],
        include: _path.src,
      },
    ],
  },
};
