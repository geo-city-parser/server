import { _CONSOLE, _NODE } from '@access';
//
// import { HandleRejectedPromise } from './utils/HandleRejectedPromise';

// import { Mongodb } from './utils/Mongodb';
import { ColoredConsole } from './utils/ColoredConsole';

const { bug } = _CONSOLE;

export const RunApp = async (app: any) => {
  try {
    const { HOST, PORT } = _NODE;

    // await Mongodb();
    return app.listen(PORT, HOST, ColoredConsole);
  } catch (error) {
    console.error(bug(`⚡ [NODE_RunApp]:`), String(error));
    return error;
  }
};
