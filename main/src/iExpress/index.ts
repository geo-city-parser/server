/**
 * ⭐⭐⭐ SERVER CONFIG ⭐⭐⭐
 *
 * Example Node (Express + Mongoose) codebase containing real world examples
 * [https://github.com/gothinkster/node-express-realworld-example-app]
 */

/**
 * Required External Modules
 */
import express from 'express';
import cors from 'cors';
import helmet from 'helmet';

import { errorHandler } from '@middleware/middlewareError';
import { _CONSOLE } from '@access';

// eslint-disable-next-line node/no-unpublished-import
import { graphqlHTTP } from 'express-graphql';
// eslint-disable-next-line node/no-unpublished-import
import { graphqlUploadExpress } from 'graphql-upload';
import { schema } from '@schema';
import { resolve } from 'path';
import { RunApp } from './RunApp';

class Initialization {
  public env: string = '';
}

export class Server extends Initialization {
  constructor() {
    super();

    const { nodeEnv } = _CONSOLE;
    this.env = process.env.NODE_ENV ?? '';

    /**
     * Required config
     */
    nodeEnv(this.env);
    _CONSOLE.line('okGray');
    console.log('•', 'State: express loading...');

    // WARNING!!! Disable SSL Certificate
    console.log('•', 'NODE_TLS_REJECT_UNAUTHORIZED', process.env.NODE_TLS_REJECT_UNAUTHORIZED);
    // process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    // if (_ENV !== 'production') process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

    const app = express();

    /**
     *  App Configuration
     */
    app.use(helmet());
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(
      '/graphql',
      graphqlUploadExpress({ maxFileSize: 5000, maxFiles: 2 }),
      graphqlHTTP({ schema }) //
    );
    app.use(express.static(resolve(process.cwd(), 'main/src/', 'static')));
    app.use(errorHandler);

    /**
     *  Run server
     */
    RunApp(app);
  }
}
