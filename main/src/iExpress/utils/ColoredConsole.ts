import { _NODE, _CONSOLE, _MONGODB } from '@access';

export const ColoredConsole = () => {
  const { HOST, PORT, API, ENV } = _NODE;
  const { log } = console;
  const _name = process.env.npm_package_name;
  // const _git = process.env.npm_package_repository_url;

  _CONSOLE.express('SERVER IS READY!');
  _CONSOLE.line();
  log('•', 'Project:', _name ?? '__TEXT__');
  // log('•', 'Git:', _git ?? '__TEXT__');
  log('•', 'Date:', new Date().toLocaleString());
  log('•', 'Host:', `${HOST}:${PORT}`);
  log('•', 'Port:', `${PORT}`);
  log('•', 'Api:', `${API}`);
  log('•', 'Development:', `${ENV}`);
  // _CONSOLE.line();
};
