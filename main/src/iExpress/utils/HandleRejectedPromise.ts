import { _CONSOLE } from '@access';

export const HandleRejectedPromise = () => {
  /**
   * A listener that stops code execution because of an unexpected error!
   */
  const handleException = (ex: Error) => {
    const obj = { Exception: ex.message, ex };
    console.log(_CONSOLE.reject(), '\n', obj);

    throw new Error('Something bad happened!');
  };

  const handleRejectedPromise = (reason: { message: string }, promise: Promise<any>) => {
    const obj = { RejectedPromise: reason, ex: promise };
    console.log(_CONSOLE.reject(), reason.message, '\n', obj);

    throw new Error('Something bad happened!');
  };

  process.on('uncaughtException', handleException);
  process.on('unhandledRejection', handleRejectedPromise);
};
