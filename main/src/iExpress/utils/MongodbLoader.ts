// import mongoose from 'mongoose';
// import ENV from '@access/isDev';
// import mongoURL from '@access/mongoSetup';
// import { _CONSOLE, _MONGODB } from '@access';

// export const MongodbLoader = async () => {
//   const { dark, mongodb, bug, done } = _CONSOLE;

//   const mongoINI = {
//     noDelay: true,
//     useCreateIndex: true,
//     useFindAndModify: false,
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//   };

//   try {
//     const connect = await mongoose.connect(mongoURL, mongoINI);

//     if (connect) mongodb('Connected!', done(Boolean(connect)));
//     else bug('Fails connect!', Boolean(connect));
//     _CONSOLE.line('okGray');
//     console.log('::', 'Connect:', ENV ? dark(_MONGODB.connect) : dark('• Server'));
//     console.log('::', 'Url:', ENV ? dark(mongoURL) : dark('• Hidden'));
//   } catch (error) {
//     mongodb(bug(error));
//   }
// };
