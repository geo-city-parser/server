/* eslint-disable camelcase */

/**
 * @example
 * place_id: number;
 licence: string;
 osm_type: string;
 osm_id: number;
 boundingbox: string[];
 lat: string;
 lon: string;
 display_name: string;
 place_rank: number;
 category: string;
 type: string;
 importance: number;
 icon: string;
 */
export interface CITY_PARAMS {
  boundingbox: string[];
  category: string;
  display_name: string;
  icon: string;
  importance: number;
  lat: string;
  licence: string;
  lon: string;
  osm_id: number;
  osm_type: string;
  place_id: number;
  place_rank: number;
  type: string;
}

/**
 * @example
 * transfer: JSON;
 tempFolder: string;
 name: string;
 */
export interface SAVE_FILE {
  transfer: JSON;
  tempFolder: string;
  name: string;
}
/**
 * @example
 * tempFolder: string;
 name: string;
 */
export interface EMPTY_FILE {
  tempFolder: string;
  name: string;
}
