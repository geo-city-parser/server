import axios from 'axios';

export const executeJson = async (number: number) => {
  // console.log(`[LOG] number`, `<${typeof number}>`, number);
  const url = `https://polygons.openstreetmap.fr/get_geojson.py?id=${number}&params=0`;

  const request = await axios({
    method: 'GET',
    url: encodeURI(url),
  });
  const next: JSON = request.data;
  // console.log(`[LOG] next`, `<${typeof next}>`, next);

  return next;
};
