// eslint-disable-next-line node/no-unpublished-import
import { FileUpload } from 'graphql-upload';

import { createAndSaveFile, emptyFile, saveFile } from './saveManager';
import { executeJson } from './executeJson';
import { readLine } from './readLine';
import { searchCity } from './searchCity';
import { zipFile } from './zipFile';

export const logic = async (transfer: FileUpload) => {
  const { tempFolderFile, tempFolder } = await createAndSaveFile(transfer);
  const arrCites = await readLine(tempFolderFile);

  for await (const name of arrCites) {
    const cityID = await searchCity(name);
    // console.log(`[LOG] cityID`, `<${typeof cityID}>`, cityID);

    if (typeof cityID === 'number') {
      const _json = await executeJson(cityID);
      await saveFile({ transfer: _json, tempFolder, name });
    } else {
      await emptyFile({ tempFolder, name });
      console.log(`city::${name} is EMPTY`);
    }
  }

  const zipPath = await zipFile(tempFolder);
  return zipPath;
};
