import { readFile } from 'fs/promises';

export const readLine = async (url: string) => {
  const controller = new AbortController();
  const { signal } = controller;
  const str = await readFile(url, { encoding: 'utf-8', signal });
  const extract = str.match(/([ -]?[а-яА-Яa-zA-Z])+/gm);
  // console.log(`[LOG] extract`, `<${typeof extract}>`, extract);

  const usingArrayFrom: string[] = Object.assign([], extract);
  return usingArrayFrom;
};
