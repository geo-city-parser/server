import { createWriteStream } from 'fs';
import { join } from 'path';
import { killDir, mkDir, mkDirTmp } from '@helper/folderManager';
// eslint-disable-next-line node/no-unpublished-import
import { FileUpload } from 'graphql-upload';
import slugify from 'slugify';
import { EMPTY_FILE, SAVE_FILE } from './Type.d';

export const createAndSaveFile = async (transfer: FileUpload) => {
  const { createReadStream: createRS, filename } = transfer;

  // await killDir();
  await mkDir('stash');
  const tempFolder = await mkDirTmp('stash');
  const tempFolderFile = join(`${tempFolder}/${filename}`);

  // eslint-disable-next-line
  await new Promise(resolve => {
    const stream = createRS();
    stream.pipe(createWriteStream(tempFolderFile));
    stream.on('finish', resolve);
    return stream.on('end', resolve);
  });

  return { tempFolderFile, tempFolder };
};

export const saveFile = async ({ transfer, tempFolder, name }: SAVE_FILE) => {
  const convert = slugify(name, '_');
  const _path = join(`${tempFolder}/${convert.toLocaleLowerCase()}`);

  await mkDir(_path);

  // eslint-disable-next-line
  await new Promise(resolve => {
    const stream = createWriteStream(`${_path}/geo.json`, { encoding: 'utf8', flags: 'w' });
    stream.write(JSON.stringify(transfer));
    stream.on('finish', resolve);
    stream.on('end', resolve);
    return stream.end();
  });

  return _path;
};

export const emptyFile = async ({ tempFolder, name }: EMPTY_FILE) => {
  const convert = slugify(name, '_');
  const _path = join(`${tempFolder}/EMPTY-${convert.toLocaleLowerCase()}`);

  await mkDir(_path);

  return _path;
};
