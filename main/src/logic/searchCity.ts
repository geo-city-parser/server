import { $$isEmpty } from '@helper/GlobalClass/lib/IS';
import axios from 'axios';
import { CITY_PARAMS } from './Type.d';

export const searchCity = async (city: string) => {
  const url = `https://nominatim.openstreetmap.org/search.php?q=${city}&format=jsonv2`;

  const request = await axios({
    method: 'GET',
    url: encodeURI(url),
  });
  const next = request.data ?? 0;

  if (Array.isArray(next)) {
    const arr = next.filter(({ type }: CITY_PARAMS) => type === 'city');

    if ($$isEmpty(arr)) return null;

    const [{ osm_id: id = null }] = arr;
    return id as number;
  }

  return null;
};
