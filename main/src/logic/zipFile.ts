import archiver from 'archiver';
import { createWriteStream } from 'fs';
import { join } from 'path';

import crypto from 'crypto';
import { killDir, mkDir } from '@helper/folderManager';

import { _NODE } from '@access';

const { HOST, PORT } = _NODE;

export const zipFile = async (tempFolder: string) => {
  // await killDir('static');
  const url = await mkDir('static');

  const convert = crypto.randomBytes(7).toString('hex');
  const _path = join(`${url}/${convert}.zip`);

  // eslint-disable-next-line
  await new Promise(resolve => {
    const output = createWriteStream(_path);
    const archive = archiver('zip');
    archive.directory(tempFolder, false);

    output.on('close', () => {
      console.log(`${archive.pointer()} total bytes`);
      // console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    archive.on('error', err => {
      throw err;
    });

    archive.pipe(output);
    archive.finalize();

    archive.on('end', resolve);
  });

  return new URL('', join(`http://${HOST}:${PORT}`, `${convert}.zip`));
};
