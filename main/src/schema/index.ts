// eslint-disable-next-line node/no-unpublished-import
import { GraphQLUpload, FileUpload } from 'graphql-upload';
// eslint-disable-next-line node/no-unpublished-import
import { makeExecutableSchema } from '@graphql-tools/schema';

import { logic } from '@logic';

interface UPLOAD_FILE {
  file: Promise<FileUpload>;
}

const typeDefs = `
  scalar File {
    upload: String
  }
  
  scalar Upload

  type Any {
    api: String
  }

  type Query {
    api: String
  }

  type Mutation {
    uploadFiles(file: ${GraphQLUpload}): String
  }
`;

const resolvers = {
  Query: {
    api: () => 'api-test',
  },
  Upload: GraphQLUpload,

  Mutation: {
    uploadFiles: async (_: any, { file }: UPLOAD_FILE) => {
      try {
        const transfer = await file;
        const result = await logic(transfer);

        return result;
      } catch (error) {
        console.log(`[LOG] error`, `<${typeof error}>`, error);
        return error;
      }
    },
  },
};
export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});
